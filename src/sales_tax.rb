require 'bigdecimal'
require_relative 'product'

class SalesTax
  TAXABLE_PRODUCT_TYPES = [:music]
  NOT_TAXABLE_PRODUCT_TYPES = [:book, :food, :medical]

  attr_reader :product, :total

  def initialize(product)
    @original_product_price = product.price
    @product = product
    @total = BigDecimal.new(0, 4)
  end

  def calculate
    product.price += percent_from_price(percents: 10) unless NOT_TAXABLE_PRODUCT_TYPES.include?(product.type.id)
    product.price += percent_from_price(percents: 5) if product.type.imported?
    product.price = product.price.round(2, BigDecimal::ROUND_HALF_UP)
    @total = product.price - @original_product_price
    self
  end

  def percent_from_price(percents:)
    @original_product_price / 100 * percents
  end
end