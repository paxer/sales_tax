class ProductType
  attr_reader :product

  TYPES = [:book, :food, :medical, :music, :imported]

  def initialize(product)
    @product = product
  end

  def id
    TYPES.each do |type|
      return type if send("#{type}?")
    end
  end

  def book?
    product.name.match(/book/)
  end

  def food?
    product.name.match(/chocolate/)
  end

  def medical?
    product.name.match(/pills/)
  end

  def music?
    product.name.match(/music/)
  end

  def imported?
    product.name.match(/imported/)
  end
end