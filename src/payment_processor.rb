require_relative 'sales_tax'
require 'bigdecimal'

class PaymentProcessor
  attr_reader :purchased_products, :total, :sales_tax_total

  def initialize
    @purchased_products = []
    @total = BigDecimal.new(0, 4)
    @sales_tax_total = BigDecimal.new(0, 4)
  end

  def purchase(products_to_purchase)
    products_to_purchase.each do |product|
      sales_tax = SalesTax.new(product)
      purchased_products << sales_tax.calculate.product
      @sales_tax_total = (sales_tax_total + sales_tax.total).round(2, BigDecimal::ROUND_HALF_UP)
      @total = (total + product.price).round(2, BigDecimal::ROUND_HALF_UP)
    end
    self
  end
end