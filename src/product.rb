require 'bigdecimal'
require_relative 'product_type'

class Product
  attr_accessor :quantity, :name, :price

  def initialize(quantity:, name:, price:)
    @quantity = quantity
    @name = name
    @price = BigDecimal.new(price, 4)
  end

  def type
    ProductType.new(self)
  end
end