class Receipt
  def print(payment_processor)
    result = payment_processor.purchased_products.inject('') do |result, product|
      result +="#{product.quantity}, #{product.name}, #{'%.2f' % product.price}\n "
    end
    "#{result.rstrip}\n\nSales Taxes: #{'%.2f' % payment_processor.sales_tax_total}\nTotal: #{'%.2f' % payment_processor.total}"
  end
end