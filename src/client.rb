require_relative 'shopping_basket'
require_relative 'payment_processor'
require_relative 'receipt'

class Client
  def buy_products(products)
    products_to_purchase = ShoppingBasket.new.add_products(products).products
    payment_processor = PaymentProcessor.new.purchase(products_to_purchase)
    Receipt.new.print(payment_processor)
  end
end