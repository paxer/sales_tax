require 'csv'
require_relative 'product'

class ShoppingBasket
  attr_reader :products

  def initialize
    @products = []
  end

  def add_products(products_details)
    CSV.parse(products_details, headers: true) do |row|
      @products << Product.new(quantity: row['Quantity'], name: row['Product'], price: row['Price'])
    end
    self
  end
end