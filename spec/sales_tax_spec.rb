require_relative '../src/sales_tax'
require_relative '../src/product'

RSpec.describe SalesTax do
  let(:book) { Product.new(quantity: 1, name: 'book', price: 12.49) }
  let(:music_cd) { Product.new(quantity: 1, name: 'music cd', price: 14.99) }
  let(:chocolate_bar) { Product.new(quantity: 1, name: 'chocolate bar', price: 0.85) }
  let(:imported_bottle_of_perfume) { Product.new(quantity: 1, name: 'imported bottle of perfume', price: 47.50) }
  let(:packet_of_headache_pills) { Product.new(quantity: 1, name: 'packet of headache pills', price: 9.75) }


  context 'does not apply 10%' do
    it 'on books' do
      sales_tax = SalesTax.new(book)
      expect(sales_tax.calculate.product.price).to eql BigDecimal.new(12.49, 4)
    end

    it 'on food' do
      sales_tax = SalesTax.new(chocolate_bar)
      expect(sales_tax.calculate.product.price).to eql BigDecimal(0.85, 4)
    end

    it 'on medical products' do
      sales_tax = SalesTax.new(packet_of_headache_pills)
      expect(sales_tax.calculate.product.price).to eql BigDecimal(9.75, 4)
    end
  end

  context 'applies 10%' do
    it 'on music_cd' do
      sales_tax = SalesTax.new(music_cd)
      expect(sales_tax.calculate.product.price).to eql BigDecimal(16.49, 4)
    end

    context 'applies additional 5% for imported products' do
      it 'on imported_bottle_of_perfume' do
        sales_tax = SalesTax.new(imported_bottle_of_perfume)
        expect(sales_tax.calculate.product.price).to eql BigDecimal(54.65, 4)
      end
    end
  end

  context '#total' do
    it 'calculates sales tax total' do
      sales_tax = SalesTax.new(imported_bottle_of_perfume)
      expect(sales_tax.calculate.total).to eql BigDecimal(7.36, 4)
    end
  end
end