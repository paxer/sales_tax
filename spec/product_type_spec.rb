require_relative '../src/product'

RSpec.describe Product do
  let(:book) { Product.new(quantity: 1, name: 'book', price: 1.11) }
  let(:product_type) { ProductType.new(book) }

  context '#id' do
    it 'detects books' do
      expect(product_type.id).to be :book
    end
  end

  # etc
end