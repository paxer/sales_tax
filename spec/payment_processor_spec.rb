require_relative '../src/payment_processor'
require_relative '../src/product'

RSpec.describe PaymentProcessor do
  let(:products_to_purchase) { [Product.new(quantity: 1, name: 'book', price: 12.49), Product.new(quantity: 2, name: 'music cd', price: 14.99)] }

  describe '#purchase' do
    subject { PaymentProcessor.new.purchase(products_to_purchase) }

    it 'create a list of purchased products' do
      expect(subject.purchased_products).to be_kind_of(Array)
      expect(subject.purchased_products.first).to be_kind_of(Product)
    end
  end
end