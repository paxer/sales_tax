require_relative '../src/shopping_basket.rb'

RSpec.describe ShoppingBasket do
  let(:product_details) { "Quantity,Product,Price,\n1,book,12.49,\n1,music cd,14.99,\n1,chocolate bar,0.85" }

  describe '#add_products' do
    context 'with input 1' do
      it 'creates an array' do
        expect(subject.add_products(product_details).products).to be_kind_of(Array)
      end

      it 'creates an array of Product type' do
        expect(subject.add_products(product_details).products.first).to be_kind_of(Product)
      end

      it 'contains all products' do
        expect(subject.add_products(product_details).products.size).to be 3
      end
    end
  end
end