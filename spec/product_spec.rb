require_relative '../src/product'

RSpec.describe Product do
  let(:product) { Product.new(quantity: 1, name: 'Product', price: 1.11) }

  it 'has a name accessor' do
    expect(product.name).to eq 'Product'
  end

  it 'has a quantity accessor' do
    expect(product.quantity).to be 1
  end

  context 'price' do
    it 'has an accessor' do
      expect(product.price.to_f).to be 1.11
    end

    it 'is a BigDecimal' do
      expect(product.price).to be_kind_of(BigDecimal)
    end
  end
end