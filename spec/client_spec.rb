require_relative '../src/client'

RSpec.describe Client do
  let(:input1) { "Quantity,Product,Price,\n1,book,12.49,\n1,music cd,14.99,\n1,chocolate bar,0.85" }
  let(:input2) { "Quantity,Product,Price,\n1,imported box of chocolates,10.00,\n1,imported bottle of perfume,47.50" }
  let(:input3) { "Quantity,Product,Price,\n1,imported bottle of perfume,27.99,\n1,bottle of perfume,18.99\n1,packet of headache pills,9.75\n1,box of imported chocolates,11.25" }

  describe '#buy_products' do
    context 'with input 1' do
      it 'prints out the receipt details' do
        expect(subject.buy_products(input1)).to eq "1, book, 12.49\n 1, music cd, 16.49\n 1, chocolate bar, 0.85\n\nSales Taxes: 1.50\nTotal: 29.83"
      end
    end

    context 'with input 2' do
      it 'prints out the receipt details' do
        expect(subject.buy_products(input2)).to eq "1, imported box of chocolates, 10.50\n 1, imported bottle of perfume, 54.65\n\nSales Taxes: 7.65\nTotal: 65.15"
      end
    end

    context 'with input 3' do
      it 'prints out the receipt details' do
        expect(subject.buy_products(input3)).to eq "1, imported bottle of perfume, 32.19\n 1, bottle of perfume, 20.89\n 1, packet of headache pills, 9.75\n 1, imported box of chocolates, 11.85\n\nSales Taxes: 7.65\nTotal: 65.15"
      end
    end
  end
end